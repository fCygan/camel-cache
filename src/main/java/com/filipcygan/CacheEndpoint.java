package com.filipcygan;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public interface CacheEndpoint {

    @POST
    @Path("/")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    String saveCache(String cachedString);

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_PLAIN)
    String getCache();

}
