package com.filipcygan;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cache.CacheConstants;
import org.springframework.stereotype.Component;

@Component
public class CacheRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("cxfrs:bean:cacheTestingEndpoint").choice()
                .when(header("operationName").isEqualTo("saveCache"))
                .to("direct:post")
                .when(header("operationName").isEqualTo("getCache"))
                .to("direct:get");

        from("direct:post")
                .setHeader(CacheConstants.CACHE_OPERATION, constant(CacheConstants.CACHE_OPERATION_UPDATE))
                .setHeader(CacheConstants.CACHE_KEY, constant("Ralph_Waldo_Emerson"))
                .to("cache://cacheTest?timeToLiveSeconds=10&timeToIdleSeconds=30").to("direct:out");

        from("direct:get")
                .setHeader(CacheConstants.CACHE_OPERATION, constant(CacheConstants.CACHE_OPERATION_GET))
                .setHeader(CacheConstants.CACHE_KEY, constant("Ralph_Waldo_Emerson"))
                .to("cache://cacheTest").to("direct:out");

        from("direct:out")
                .marshal().string();
    }
}
